# Release history

## 0.4.1 (2021-04-27)

* Add pypi deploy runner.

## 0.4.0 (2021-03-10)

* Add autocompletion.

## 0.3.0 (2021-02-18)

* Add a config file to store information about the host and PAT.

## 0.2.1 (2021-02-03)

* Correct notation of licence.

## 0.2.0 (2021-02-03)

* Add more CLI tools for communicating with an elabFTW instance.

## 0.1.0 (2021-01-28)

* Provide some CLI commands to interact with an elabFTW instance via eLabFTW's API.
