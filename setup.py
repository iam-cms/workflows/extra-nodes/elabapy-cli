# This file is a part of elabftwapy-cli to interact with an elabFTW instance via a CLI
# Copyright (C) 2021 Karlsruhe Institute of Technology
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import os

from setuptools import find_packages
from setuptools import setup


with open(os.path.join("elabapy_cli", "version.py"), encoding="utf-8") as f:
    exec(f.read())


with open("README.md", encoding="utf-8") as f:
    long_description = f.read()


setup(
    name="elabapy-cli",
    version=__version__,
    license="GNU GPL-3.0",
    author="Karlsruhe Institute of Technology",
    description="eLabFTW CLI library.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/iam-cms/workflows/extra-nodes/elabapy-cli",
    packages=find_packages(),
    include_package_data=True,
    python_requires=">=3.6",
    zip_safe=False,
    classifiers=[
        "Development Status :: 4 - Beta",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
    ],
    install_requires=[
        "Click>=7.1.1,<8.0.0",
        "click_completion>=0.5.2",
        "requests>=2.22.0",
        "xmlhelpy>=0.2.0",
        "elabapy>=0.7.0",
    ],
    extras_require={
        "dev": [
            "black==20.8b1",
            "pre-commit>=2.7.0",
            "pylint>=2.4.4,<2.5.0",
            "tox>=3.15.0",
        ]
    },
    entry_points={"console_scripts": ["elabapy=elabapy_cli.main:elabapy"]},
)
